package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.ShopSlot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("shopslotRepository")
public interface ShopSlotRepository<T> extends CrudRepository<ShopSlot,
        Integer> {

}
