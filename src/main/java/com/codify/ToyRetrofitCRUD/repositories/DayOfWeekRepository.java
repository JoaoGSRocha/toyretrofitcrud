package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.DayOfWeek;
import com.codify.ToyRetrofitCRUD.entities.DaySlot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("dayofweekRepository")
public interface DayOfWeekRepository<T> extends CrudRepository<DayOfWeek,
        Integer> {
}
