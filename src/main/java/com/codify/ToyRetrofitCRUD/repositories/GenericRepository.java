package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.Appointment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenericRepository<T> extends CrudRepository<Appointment,
        Integer> {
}
