package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("clientRepository")
public interface ClientRepository<T> extends CrudRepository<Client, Integer> {
}
