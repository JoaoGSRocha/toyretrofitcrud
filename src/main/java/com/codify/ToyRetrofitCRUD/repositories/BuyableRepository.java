package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.Buyable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BuyableRepository<T> extends
        CrudRepository<Buyable, Integer> {
}
