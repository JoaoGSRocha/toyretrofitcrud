package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.Client;
import com.codify.ToyRetrofitCRUD.entities.DaySlot;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("dayslotRepository")
public interface DaySlotRepository<T> extends CrudRepository<DaySlot, Integer> {
}
