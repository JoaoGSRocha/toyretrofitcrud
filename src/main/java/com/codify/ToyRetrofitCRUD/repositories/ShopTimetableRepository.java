package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.ShopSlot;
import com.codify.ToyRetrofitCRUD.entities.ShopTimetable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("shoptimeRepository")
public interface ShopTimetableRepository<T>
        extends CrudRepository<ShopTimetable, Integer> {

}
