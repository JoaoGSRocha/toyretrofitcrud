package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.Shop;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("shopRepository")
public interface ShopRepository<T> extends
        CrudRepository<Shop, Integer> {
}
