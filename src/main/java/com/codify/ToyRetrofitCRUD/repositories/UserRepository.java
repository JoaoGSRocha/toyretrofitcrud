package com.codify.ToyRetrofitCRUD.repositories;

import com.codify.ToyRetrofitCRUD.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository("userRepository")
public interface UserRepository<T> extends CrudRepository<User, Integer> {

}
