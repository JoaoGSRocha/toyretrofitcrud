package com.codify.ToyRetrofitCRUD.controllers;

import com.codify.ToyRetrofitCRUD.entities.Shop;
import com.codify.ToyRetrofitCRUD.services.GenericService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/shop")
public class ShopController {

    @Autowired
    private GenericService<Shop> shopRepository;

    @RequestMapping(value = "findall",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Iterable<Shop>> findAll() {
        try {
            return new ResponseEntity<Iterable<Shop>>(
                    shopRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<Shop>>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "find/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Shop> find(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<Shop>(
                    shopRepository.find(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.print(id +" "+e);
            return new ResponseEntity<Shop>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "create",
            method = RequestMethod.POST,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Shop> create(@RequestBody Shop shop) {
        try {
            Shop nshop = shopRepository.create(shop);
            System.out.print(new Gson().toJson(nshop).toString());
            return new ResponseEntity<Shop>(nshop,
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Shop>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "update",
            method = RequestMethod.PUT,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Void> update(@RequestBody Shop shop) {
        try {
            shopRepository.update(shop);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE
            )
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            shopRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }
}
