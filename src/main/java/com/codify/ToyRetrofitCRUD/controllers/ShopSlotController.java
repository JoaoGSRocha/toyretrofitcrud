package com.codify.ToyRetrofitCRUD.controllers;

import com.codify.ToyRetrofitCRUD.entities.Shop;
import com.codify.ToyRetrofitCRUD.entities.ShopSlot;
import com.codify.ToyRetrofitCRUD.services.GenericService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/shopslot")
public class ShopSlotController {

    @Autowired
    private GenericService<ShopSlot> shopSloRepository;

    @RequestMapping(value = "findall",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Iterable<ShopSlot>> findAll() {
        try {
            return new ResponseEntity<Iterable<ShopSlot>>(
                    shopSloRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<ShopSlot>>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "find/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<ShopSlot> find(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<ShopSlot>(
                    shopSloRepository.find(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<ShopSlot>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "create",
            method = RequestMethod.POST,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<ShopSlot> create(@RequestBody ShopSlot shopSlot) {
        try {
            ShopSlot nshopslot = shopSloRepository.create(shopSlot);
            System.out.print(new Gson().toJson(nshopslot));
            return new ResponseEntity<ShopSlot>(nshopslot, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<ShopSlot>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "update",
            method = RequestMethod.PUT,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Void> update(@RequestBody ShopSlot shopSlo) {
        try {
            shopSloRepository.update(shopSlo);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE
            )
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            shopSloRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }
}
