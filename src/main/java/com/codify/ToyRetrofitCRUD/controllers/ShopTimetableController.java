package com.codify.ToyRetrofitCRUD.controllers;

import com.codify.ToyRetrofitCRUD.entities.ShopTimetable;
import com.codify.ToyRetrofitCRUD.services.GenericService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/shoptime")
public class ShopTimetableController {

    @Autowired
    private GenericService<ShopTimetable> shopSloRepository;

    @RequestMapping(value = "findall",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Iterable<ShopTimetable>> findAll() {
        try {
            return new ResponseEntity<Iterable<ShopTimetable>>(
                    shopSloRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<ShopTimetable>>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "find/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<ShopTimetable> find(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<ShopTimetable>(
                    shopSloRepository.find(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<ShopTimetable>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "create",
            method = RequestMethod.POST,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<ShopTimetable> create(@RequestBody ShopTimetable shopTime) {
        try {
            ShopTimetable nshopslot = shopSloRepository.create(shopTime);
            System.out.print(new Gson().toJson(nshopslot));
            return new ResponseEntity<ShopTimetable>(nshopslot, HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<ShopTimetable>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "update",
            method = RequestMethod.PUT,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Void> update(@RequestBody ShopTimetable shopSlo) {
        try {
            shopSloRepository.update(shopSlo);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE
            )
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            shopSloRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }
}
