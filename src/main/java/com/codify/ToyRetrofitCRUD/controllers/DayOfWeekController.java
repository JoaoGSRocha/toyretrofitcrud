package com.codify.ToyRetrofitCRUD.controllers;

import com.codify.ToyRetrofitCRUD.entities.DayOfWeek;
import com.codify.ToyRetrofitCRUD.services.GenericService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/dayofweek")
public class DayOfWeekController {

    @Autowired
    private GenericService<DayOfWeek> dayOfWeekRepository;

    @RequestMapping(value = "findall",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Iterable<DayOfWeek>> findAll() {
        try {
            return new ResponseEntity<Iterable<DayOfWeek>>(
                    dayOfWeekRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<DayOfWeek>>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "find/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<DayOfWeek> find(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<DayOfWeek>(
                    dayOfWeekRepository.find(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.print(id +" "+e);
            return new ResponseEntity<DayOfWeek>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "create",
            method = RequestMethod.POST,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<DayOfWeek> create(@RequestBody DayOfWeek dayOfWeek) {
        try {
            DayOfWeek ndayOfWeek = dayOfWeekRepository.create(dayOfWeek);
            System.out.print(new Gson().toJson(ndayOfWeek).toString());
            return new ResponseEntity<DayOfWeek>(ndayOfWeek,
                    HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<DayOfWeek>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "update",
            method = RequestMethod.PUT,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Void> update(@RequestBody DayOfWeek dayOfWeek) {
        try {
            dayOfWeekRepository.update(dayOfWeek);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE
            )
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            dayOfWeekRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }
}
