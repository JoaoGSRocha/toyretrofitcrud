package com.codify.ToyRetrofitCRUD.controllers;

import com.codify.ToyRetrofitCRUD.entities.Appointment;
//import com.codify.ToyRetrofitCRUD.services.AppointmentService;
import com.codify.ToyRetrofitCRUD.entities.Barber;
import com.codify.ToyRetrofitCRUD.services.GenericService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/appointment")
public class AppointmentController {

    @Autowired
    private GenericService<Appointment> appointmentService;

    @RequestMapping(value = "findall",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Iterable<Appointment>> findAll() {
        try {
            return new ResponseEntity<Iterable<Appointment>>(
                    appointmentService.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<Appointment>>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "find/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Appointment> find(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<Appointment>(
                    appointmentService.find(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Appointment>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "create",
            method = RequestMethod.POST,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Appointment> create(@RequestBody Appointment appointment) {
        try {
            Appointment nappointment = appointmentService.create(appointment);
            System.out.print(new Gson().toJson(nappointment).toString());
            return new ResponseEntity<Appointment>(nappointment, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Appointment>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "update",
            method = RequestMethod.PUT,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Void> update(@RequestBody Appointment appointment) {
        try {
            appointmentService.update(appointment);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE
            )
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            appointmentService.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }
}
