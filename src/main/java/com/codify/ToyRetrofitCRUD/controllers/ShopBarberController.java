package com.codify.ToyRetrofitCRUD.controllers;

import com.codify.ToyRetrofitCRUD.entities.ShopBarber;
import com.codify.ToyRetrofitCRUD.repositories.ShopBarberRepository;
import com.codify.ToyRetrofitCRUD.services.GenericService;
import com.codify.ToyRetrofitCRUD.services.ShopBarberService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/shopbarber")
public class ShopBarberController {

    @Autowired
    private ShopBarberService shopbarberRepository;

    @RequestMapping(value = "findall",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")

    public ResponseEntity<Iterable<ShopBarber>> findAll() {
        try {
            return new ResponseEntity<Iterable<ShopBarber>>(
                   shopbarberRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Iterable<ShopBarber>>(
                    HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value = "find/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<ShopBarber> find(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<ShopBarber>(
                    shopbarberRepository.find(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<ShopBarber>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "findById/{id}",
            method = RequestMethod.GET,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Iterable<ShopBarber>> findById(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<Iterable<ShopBarber>>(
                    shopbarberRepository.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            System.out.print(e);
            return new ResponseEntity<Iterable<ShopBarber>>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "create",
            method = RequestMethod.POST,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<ShopBarber> create(@RequestBody ShopBarber shopbarber) {
        try {

            ShopBarber nshopbarber  = shopbarberRepository.create(shopbarber);
            System.out.print(new Gson().toJson(nshopbarber).toString());
            return new ResponseEntity<ShopBarber>(nshopbarber, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<ShopBarber>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "update",
            method = RequestMethod.PUT,
            produces =  {MimeTypeUtils.APPLICATION_JSON_VALUE},
            consumes = { MimeTypeUtils.APPLICATION_JSON_VALUE},
            headers = "Accept=application/json")
    public ResponseEntity<Void> update(@RequestBody ShopBarber shopbarber) {
        try {
            shopbarberRepository.update(shopbarber);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "delete/{id}",
            method = RequestMethod.DELETE
    )
    public ResponseEntity<Void> delete(@PathVariable("id") String id) {
        try {
            shopbarberRepository.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Void>(
                    HttpStatus.BAD_REQUEST);
        }
    }
}
