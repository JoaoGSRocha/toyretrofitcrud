package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.ShopSlot;
import com.codify.ToyRetrofitCRUD.repositories.ShopSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("shopslotService")
public class ShopSlotServiceImpl implements GenericService<ShopSlot> {

    @Autowired
    private ShopSlotRepository shopSlotRepository;

    @Override
    public Iterable<ShopSlot> findAll() { return shopSlotRepository.findAll(); }

    @Override
    public ShopSlot find(Integer id) {
        return (ShopSlot) shopSlotRepository.findById(id).get();
    }

    @Override
    public ShopSlot create(ShopSlot shopSlot) { return  (ShopSlot) shopSlotRepository.save(shopSlot); }

    @Override
    public void update(ShopSlot shopSlot) { shopSlotRepository.save(shopSlot); }

    @Override
    public void delete(String id) { shopSlotRepository.delete(id); }
}
