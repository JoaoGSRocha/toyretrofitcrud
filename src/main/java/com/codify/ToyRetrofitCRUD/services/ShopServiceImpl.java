package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Shop;
import com.codify.ToyRetrofitCRUD.repositories.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("shopService")
public class ShopServiceImpl implements GenericService<Shop> {

    @Autowired
    private ShopRepository shopRepository;


    @Override
    public Iterable<Shop> findAll() { return shopRepository.findAll(); }

    @Override
    public Shop find(Integer id) {
        return (Shop) shopRepository.findById(id).get();
    }

    @Override
    public Shop create(Shop shop) { return  (Shop) shopRepository.save(shop); }

    @Override
    public void update(Shop shop) { shopRepository.save(shop); }

    @Override
    public void delete(String id) { shopRepository.deleteById(id); }
}
