package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Buyable;
import com.codify.ToyRetrofitCRUD.repositories.BuyableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

@Service("buyableService")
public class BuyableServiceImpl implements GenericService<Buyable> {

    @Autowired
    private BuyableRepository buyableRepository;

    @Override
    public Iterable<Buyable> findAll() { return buyableRepository.findAll(); }

    @Override
    public Buyable find(Integer id) {
        return (Buyable) buyableRepository.findById(id).get();
    }

    @Override
    public Buyable create(Buyable buyable) { return  (Buyable) buyableRepository.save(buyable); }

    @Override
    public void update(Buyable buyable) { buyableRepository.save(buyable); }

    @Override
    public void delete(String id) { buyableRepository.delete(id); }
}
