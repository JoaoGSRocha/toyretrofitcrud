package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Appointment;
import com.codify.ToyRetrofitCRUD.entities.Buyable;
import com.codify.ToyRetrofitCRUD.repositories.GenericRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("appointmentService")
public class AppointmentServiceImpl implements GenericService<Appointment> {

    @Autowired
    private GenericRepository appointmentRepository;

    @Override
    public Iterable<Appointment> findAll() {
        return appointmentRepository.findAll();
    }

    @Override
    public Appointment find(Integer id) {
        return (Appointment) appointmentRepository.findById(id).get();
    }

    @Override
    public Appointment create(Appointment o) {
        return (Appointment) appointmentRepository.save(o);
    }


    @Override
    public void update(Appointment o) {
        appointmentRepository.save(o);
    }

    @Override
    public void delete(String id){
        appointmentRepository.deleteById(id);
    }
}

