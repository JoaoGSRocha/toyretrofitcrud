package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Client;
import com.codify.ToyRetrofitCRUD.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("clientService")
public class ClientServiceImpl implements GenericService<Client> {

    @Autowired
    private ClientRepository clientRepository;


    @Override
    public Iterable<Client> findAll() { return clientRepository.findAll(); }

    @Override
    public Client find(Integer id) {
        return (Client) clientRepository.findById(id).get();
    }

    @Override
    public Client create(Client client) { return  (Client) clientRepository.save(client); }

    @Override
    public void update(Client client) { clientRepository.save(client); }

    @Override
    public void delete(String id) { clientRepository.deleteById(id); }
}
