package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Barber;
import com.codify.ToyRetrofitCRUD.repositories.BarberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("barberService")
public class BarberServiceImpl implements GenericService<Barber> {

    @Autowired
    private BarberRepository barberRepository;

    @Override
    public Iterable<Barber> findAll() { return barberRepository.findAll(); }

    @Override
    public Barber find(Integer id) {
        return (Barber) barberRepository.findById(id).get();
    }

    @Override
    public Barber create(Barber barber) { return (Barber) barberRepository.save(barber); }

    @Override
    public void update(Barber barber) { barberRepository.save(barber); }

    @Override
    public void delete(String id) { barberRepository.deleteById(id); }
}
