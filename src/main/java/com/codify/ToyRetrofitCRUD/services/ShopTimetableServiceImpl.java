package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.ShopTimetable;
import com.codify.ToyRetrofitCRUD.entities.ShopTimetable;
import com.codify.ToyRetrofitCRUD.repositories.ShopTimetableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("shoptimeService")
public class ShopTimetableServiceImpl implements GenericService<ShopTimetable> {

    @Autowired
    private ShopTimetableRepository shopTimeRepository;

    @Override
    public Iterable<ShopTimetable> findAll() {
        return shopTimeRepository.findAll();
    }

    @Override
    public ShopTimetable find(Integer id) {
        return (ShopTimetable) shopTimeRepository.findById(id).get();
    }

    @Override
    public ShopTimetable create(ShopTimetable shopTime) { 
        return  (ShopTimetable) shopTimeRepository.save(shopTime); 
    }

    @Override
    public void update(ShopTimetable shopTime) {
        shopTimeRepository.save(shopTime);
    }

    @Override
    public void delete(String id) { shopTimeRepository.delete(id); }
}
