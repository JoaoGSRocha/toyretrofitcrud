package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.DaySlot;
import com.codify.ToyRetrofitCRUD.repositories.DaySlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("dayslotService")
public class DaySlotServiceImpl implements GenericService<DaySlot> {

    @Autowired
    private DaySlotRepository daySlotRepository;

    @Override
    public Iterable<DaySlot> findAll() {
        return daySlotRepository.findAll();
    }

    @Override
    public DaySlot find(Integer id) {
        return (DaySlot) daySlotRepository.findById(id).get();
    }

    @Override
    public DaySlot create(DaySlot shopBarber) {
        return (DaySlot) daySlotRepository.save(shopBarber);
    }

    @Override
    public void update(DaySlot shopBarber) {
        daySlotRepository.save(shopBarber);
    }

    @Override
    public void delete(String id) { daySlotRepository.delete(id); }
}
