package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.ShopBarber;

public interface GenericService<T> {

   public Iterable<T> findAll();

   public T  find(Integer id);

   public T create(T t);

   public void update(T t);

   public void delete(String id);

}
