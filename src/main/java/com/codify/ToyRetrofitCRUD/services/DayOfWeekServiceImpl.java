package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.DayOfWeek;
import com.codify.ToyRetrofitCRUD.repositories.DayOfWeekRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("dayofweekService")
public class DayOfWeekServiceImpl implements GenericService<DayOfWeek> {

    @Autowired
    private DayOfWeekRepository dayOfWeekRepository;

    @Override
    public Iterable<DayOfWeek> findAll() {
        return dayOfWeekRepository.findAll();
    }

    @Override
    public DayOfWeek find(Integer id) {
        return (DayOfWeek) dayOfWeekRepository.findById(id).get();
    }

    @Override
    public DayOfWeek create(DayOfWeek shopBarber) {
        return (DayOfWeek) dayOfWeekRepository.save(shopBarber);
    }

    @Override
    public void update(DayOfWeek shopBarber) {
        dayOfWeekRepository.save(shopBarber);
    }

    @Override
    public void delete(String id) { dayOfWeekRepository.delete(id); }
}
