package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Shop;
import com.codify.ToyRetrofitCRUD.entities.ShopBarber;
import com.codify.ToyRetrofitCRUD.repositories.ShopBarberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service("shopbarberService")
public class ShopBarberServiceImpl implements ShopBarberService {

    @Autowired
    private ShopBarberRepository shopBarberRepository;

    @Override
    public Iterable<ShopBarber> findAll() {
        return shopBarberRepository.findAll();
    }

    @Override
    public ShopBarber find(Integer id) {
        return (ShopBarber) shopBarberRepository.findById(id).get();
    }

    @Override
    public ShopBarber create(ShopBarber shopBarber) {
        return (ShopBarber) shopBarberRepository.save(shopBarber);
    }

    @Override
    public void update(ShopBarber shopBarber) {
        shopBarberRepository.save(shopBarber);
    }

    @Override
    public void delete(String id) { shopBarberRepository.delete(id); }

    @Override
    public Iterable<ShopBarber> findById(Integer id) {
        return (Iterable<ShopBarber>) shopBarberRepository.findById(id).get();
    }
}
