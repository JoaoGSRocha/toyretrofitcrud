package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.ShopBarber;

public interface ShopBarberService extends GenericService<ShopBarber> {

    public Iterable<ShopBarber> findById(Integer id);

}
