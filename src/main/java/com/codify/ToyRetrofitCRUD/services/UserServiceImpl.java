package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.User;
import com.codify.ToyRetrofitCRUD.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements GenericService<User> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Iterable<User> findAll() { return userRepository.findAll(); }

    @Override
    public User find(Integer id) {
        return (User) userRepository.findById(id).get();
    }

    @Override
    public User create(User user) { return  (User) userRepository.save(user); }

    @Override
    public void update(User user) { userRepository.save(user); }

    @Override
    public void delete(String id) { userRepository.delete(id); }
}
