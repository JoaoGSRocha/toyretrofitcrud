package com.codify.ToyRetrofitCRUD.services;

import com.codify.ToyRetrofitCRUD.entities.Location;

public interface LocationService {
    public Iterable<Location> findAll();
}
