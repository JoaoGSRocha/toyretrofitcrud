package com.codify.ToyRetrofitCRUD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ToyRetrofitCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ToyRetrofitCrudApplication.class, args);
	}
}
