package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "shopbarber")
public class ShopBarber implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @SerializedName("shopbarberid")
    private int shopbarberid;

    @SerializedName("barberid")
    private int barberid;

    @SerializedName("shopid")
    private int shopid;

    public int getShopbarberid() { return shopbarberid; }

    public void setShopbarberid(int shopbarberid) {
        this.shopbarberid = shopbarberid; }

    public int getShopid() { return shopid; }

    public void setShopid(int shopid) { this.shopid = shopid; }

    public int getBarberid() { return barberid; }

    public void setBarberid(int barberid) { this.barberid = barberid; }

}
