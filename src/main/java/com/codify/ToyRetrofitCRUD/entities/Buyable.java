package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "buyable")
public class Buyable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SerializedName("buyableid")
    private int buyableid;
    @SerializedName("name")
    private String name;
    @SerializedName("price")
    private float price;
    @SerializedName("discount")
    private float discount;
    @SerializedName("time")
    private Long time;

    public int getBuyableid() { return buyableid; }

    public void setBuyableid(int buyableid) { this.buyableid = buyableid; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public float getPrice() { return price; }

    public void setPrice(float price) { this.price = price; }

    public float getDiscount() { return discount; }

    public void setDiscount(float discount) { this.discount = discount; }

    public Long getTime() { return time; }

    public void setTime(Long time) { this.time = time; }
}
