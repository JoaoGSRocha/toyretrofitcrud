package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

@Entity
@Table(name = "dayofweek")
public class DayOfWeek {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @SerializedName("dayid")
    private Integer dayid;

    @SerializedName("dayofweek")
    private String dayofweek;

    public Integer getDayid() { return dayid; }

    public void setDayid(Integer dayid) { this.dayid = dayid; }

    public String getDayofweek() { return dayofweek; }

    public void setDayofweek(String dayofweek) { this.dayofweek = dayofweek; }
}
