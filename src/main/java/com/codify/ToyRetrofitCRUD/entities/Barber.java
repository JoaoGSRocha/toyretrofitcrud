package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "barber")
public class Barber implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @SerializedName("barberid")
    private Integer barberid;

    @SerializedName("name")
    private String name;

    @SerializedName("alsohome")
    private int alsohome;

    @SerializedName("userid")
    private int userid;

    public Integer getBarberid() {
        return barberid;
    }

    public void setBarberid(Integer barberid) {
        this.barberid = barberid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlsohome() {
        return alsohome;
    }

    public void setAlsohome(int alsohome) {
        this.alsohome = alsohome;
    }

    public int getUserid() { return userid; }

    public void setUserid(int userid) { this.userid = userid; }
}
