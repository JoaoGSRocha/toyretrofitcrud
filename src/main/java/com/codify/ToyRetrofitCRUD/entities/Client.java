package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "client")
public class Client implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @SerializedName("clientid")
    private Integer clientid;

    @SerializedName("name")
    private String name;

    @SerializedName("userid")
    private int userid;

    @SerializedName("mobile")
    private int mobile;

    public Integer getClientid() {
        return clientid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getMobile() { return mobile; }

    public void setMobile(int mobile) { this.mobile = mobile; }
}
