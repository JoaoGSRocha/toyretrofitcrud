package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "appointment")
public class Appointment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @SerializedName("appointmentid")
    private int appointmentid;
    @SerializedName("shopid")
    private Integer shopid;
    @SerializedName("barberid")
    private Integer barberid;
    @SerializedName("clientid")
    private Integer clientid;
    @SerializedName("buyableid")
    private Integer buyableid;
    @SerializedName("starttime")
    private Long starttime;

    public int getAppointmentid() {
        return appointmentid;
    }

    public Integer getClientid() {
        return clientid;
    }

    public Integer getBarberid() {
        return barberid;
    }

    public Integer getBuyableid() {
        return buyableid;
    }

    public void setAppointmentid(int appointmentid) {
        this.appointmentid = appointmentid;
    }

    public void setClientid(Integer clientid) {
        this.clientid = clientid;
    }

    public void setBarberid(Integer barberid) {
        this.barberid = barberid;
    }

    public void setBuyableid(Integer buyableid) { this.buyableid = buyableid; }

    public Long getStarttime() {
        return starttime;
    }

    public void setStarttime(Long starttime) {
        this.starttime = starttime;
    }

    public Integer getShopid() { return shopid; }

    public void setShopid(Integer shopid) { this.shopid = shopid; }
}
