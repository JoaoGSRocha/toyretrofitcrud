package com.codify.ToyRetrofitCRUD.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "location")
public class Location implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int locationid;

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }



    public int getLocationid() {
        return locationid;
    }


}
