package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "shopslot")
public class ShopSlot implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SerializedName("shopslotid")
    private Integer shopslotid;

    @SerializedName("shopid")
    private Integer shopid;

    @SerializedName("slotid")
    private Integer slotid;

    @SerializedName("dayid")
    private Integer dayid;

    @SerializedName("appointmentid")
    private Integer appointmentid;

    public Integer getShopslotid() {
        return shopslotid;
    }

    public void setShopslotid(Integer shopslotid) {
        this.shopslotid = shopslotid;
    }

    public Integer getShopid() {
        return shopid;
    }

    public void setShopid(Integer shopid) {
        this.shopid = shopid;
    }

    public Integer getSlotid() {
        return slotid;
    }

    public void setSlotid(Integer slotid) {
        this.slotid = slotid;
    }

    public Integer getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(Integer appointmentid) {
        this.appointmentid = appointmentid;
    }

    public Integer getDayid() { return dayid; }

    public void setDayid(Integer dayid) { this.dayid = dayid; }
}
