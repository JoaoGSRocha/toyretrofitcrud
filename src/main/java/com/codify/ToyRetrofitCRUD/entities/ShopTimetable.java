package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "shoptimetable")
public class ShopTimetable implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SerializedName("shoptimeid")
    private Integer shoptimeid;

    @SerializedName("shopid")
    private Integer shopid;

    @SerializedName("dayid")
    private Integer dayid;

    @SerializedName("openhour")
    private long openhour;

    @SerializedName("closinghour")
    private long closinghour;

    public Integer getShoptimeid() { return shoptimeid; }

    public void setShoptimeid(Integer shoptimeid) {
        this.shoptimeid = shoptimeid;
    }

    public Integer getShopid() { return shopid; }

    public void setShopid(Integer shopid) { this.shopid = shopid; }

    public Integer getDayid() { return dayid; }

    public void setDayid(Integer dayid) { this.dayid = dayid; }

    public long getOpenhour() { return openhour; }

    public void setOpenhour(long openhour) { this.openhour = openhour; }

    public long getClosinghour() { return closinghour; }

    public void setClosinghour(long closinghour) {
        this.closinghour = closinghour;
    }
}
