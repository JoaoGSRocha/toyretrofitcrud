package com.codify.ToyRetrofitCRUD.entities;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dayslot")
public class DaySlot implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @SerializedName("slotid")
    private Integer slotid;

    @SerializedName("time")
    private String time;

    public Integer getSlotid() {
        return slotid;
    }

    public void setSlotid(Integer slotid) {
        this.slotid = slotid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
